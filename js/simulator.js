var node = document.getElementById('shower-door-simulator-canvas');
var btn = document.getElementById('saveimg');

btn.onclick = function() {
     domtoimage.toBlob(document.getElementById('shower-door-simulator-canvas'))
    .then(function(blob) {
      window.saveAs(blob, 'shower-door-simulator-canvas.png');
    });
}


$('#tab-content div').hide();
$('#tab-content div:first').show();

$('#nav li').click(function() {
    $('#nav li a').removeClass("active");
    $(this).find('a').addClass("active");
    $('#tab-content div').hide();

    var indexer = $(this).index(); //gets the current index of (this) which is #nav li
    $('#tab-content div:eq(' + indexer + ')').fadeIn(); //uses whatever index the link has to open the corresponding box 
});

jQuery(document).ready(function($) {
  $(window).load(function() {
    setTimeout(function() {
      $('#preloader').fadeOut('slow', function() {});
    }, 2000);

  });
});

var $inner_preloader = $("#inner-preloader");

/*-----Settings------*/

var layout1_wall="images/simulator-wall.png";
var layout1_texture="images/simulator-texture.png";
var layout1_color="images/simulator-color.png";
var layout1_door="images/simulator-door.png";
var layout1_clips="images/simulator-clip.png";
var layout1_handle="images/simulator-handle.png";
var layout1_slides="images/simulator-slides.png";
var layout1_brick="images/simulator-bricks.png";


var layout2_clips="images/layout2_clips.png";
var layout2_slides="images/layout2_slides.png";

var single_door_green="images/single-door-green.png";
var single_door_brown="images/single-door-brown.png";
var single_door_blue="images/single-door-blue.png";
var single_door_pink="images/single-door-pink.png";
var single_door_cyan="images/single-door-cyan.png";
var single_door_grey="images/single-door-grey.png";
var single_door_black="images/single-door-black.png";

var single_door_rain="images/single-door-rain-tex.png";
var single_door_frosted="images/single-door-frosted-tex.png";
var single_door_starfire="images/single-door-starfire-tex.png";

var single_door_tubular="images/single-door-tubular-handle.png";
var single_door_antique="images/single-door-antique-handle.png";
var single_door_mitered="images/single-door-mitered-handle.png";
var single_door_ladder="images/single-door-ladder-handle.png";
var single_door_traditional="images/single-door-traditional-handle.png";





$('.single-door').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.layouts img').removeClass("active");
        $('.layout-1 img').addClass("active");	
        $('.simulator-wall').attr('src', layout1_wall); 
		$('.simulator-texture').attr('src', layout1_texture); 
		$('.simulator-color').attr('src', layout1_color); 
		$('.simulator-door').attr('src', layout1_door); 
		$('.simulator-clip').attr('src', layout1_clips); 
		$('.simulator-handle').attr('src', layout1_handle); 
		$('.simulator-slides').attr('src', layout1_slides); 
		$('.simulator-bricks').attr('src', layout1_brick); 
});

$('.layout-1').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('#layout-gallery').attr('href', 'http://lagunashowerdoors.com/gallery/layout1'); 	
		$('.options.layouts img').removeClass("active");		
        $('.layout-1 img').addClass("active");	
        $('.simulator-wall').attr('src', layout1_wall); 
		$('.simulator-texture').attr('src', layout1_texture); 
		$('.simulator-color').attr('src', layout1_color); 
		$('.simulator-door').attr('src', layout1_door); 
		$('.simulator-clip').attr('src', layout1_clips); 
		$('.simulator-handle').attr('src', layout1_handle); 
		$('.simulator-slides').attr('src', layout1_slides); 
		$('.simulator-bricks').attr('src', layout1_brick); 
});

$('.layout-2').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('#layout-gallery').attr('href', 'http://lagunashowerdoors.com/gallery/layout2'); 		
		$('.options.layouts img').removeClass("active");
        $('.layout-2 img').addClass("active");	
        $('.simulator-wall').attr('src', layout1_wall); 
		$('.simulator-texture').attr('src', layout1_texture); 
		$('.simulator-color').attr('src', layout1_color); 
		$('.simulator-door').attr('src', layout1_door); 
		$('.simulator-clip').attr('src', layout2_clips); 
		$('.simulator-handle').attr('src', layout1_handle); 
		$('.simulator-slides').attr('src', layout2_slides); 
		$('.simulator-bricks').attr('src', layout1_brick); 
});

$('.sd-white').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.colors img').removeClass("active");
        $('.sd-white img').addClass("active");	       
		$('.simulator-door').attr('src', layout1_door); 
});

$('.sd-green').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.colors img').removeClass("active");
        $('.sd-green img').addClass("active");	       
		$('.simulator-door').attr('src', single_door_green); 
});

$('.sd-brown').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.colors img').removeClass("active");
        $('.sd-brown img').addClass("active");	       
		$('.simulator-door').attr('src', single_door_brown); 
});

$('.sd-blue').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.colors img').removeClass("active");
        $('.sd-blue img').addClass("active");	       
		$('.simulator-door').attr('src', single_door_blue); 
});

$('.sd-pink').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.colors img').removeClass("active");
        $('.sd-pink img').addClass("active");	       
		$('.simulator-door').attr('src', single_door_pink); 
});

$('.sd-cyan').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.colors img').removeClass("active");
        $('.sd-cyan img').addClass("active");	       
		$('.simulator-door').attr('src', single_door_cyan); 
});

$('.sd-grey').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.colors img').removeClass("active");
        $('.sd-grey img').addClass("active");	       
		$('.simulator-door').attr('src', single_door_grey); 
});

$('.sd-black').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.colors img').removeClass("active");
        $('.sd-black img').addClass("active");	       
		$('.simulator-door').attr('src', single_door_black); 
});

$('.sd-clear').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.textures img').removeClass("active");
        $('.sd-clear img').addClass("active");	       
		$('.simulator-texture').attr('src', layout1_texture); 
});

$('.sd-rain').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.textures img').removeClass("active");
        $('.sd-rain img').addClass("active");	       
		$('.simulator-texture').attr('src', single_door_rain); 
});

$('.sd-frosted').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.textures img').removeClass("active");
        $('.sd-frosted img').addClass("active");	       
		$('.simulator-texture').attr('src', single_door_frosted); 
});

$('.sd-starfire').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.textures img').removeClass("active");
        $('.sd-starfire img').addClass("active");	       
		$('.simulator-texture').attr('src', single_door_starfire); 
});


$('.sd-tubular').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.handles img').removeClass("active");
        $('.sd-tubular img').addClass("active");	       
		$('.simulator-handle').attr('src', single_door_tubular); 
});

$('.sd-antique').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.handles img').removeClass("active");
        $('.sd-antique img').addClass("active");	       
		$('.simulator-handle').attr('src', single_door_antique); 
});

$('.sd-mitered').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.handles img').removeClass("active");
        $('.sd-mitered img').addClass("active");	       
		$('.simulator-handle').attr('src', single_door_mitered); 
});

$('.sd-traditional').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.handles img').removeClass("active");
        $('.sd-traditional img').addClass("active");	       
		$('.simulator-handle').attr('src', single_door_traditional); 
});

$('.sd-ladder').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.handles img').removeClass("active");
        $('.sd-ladder img').addClass("active");	       
		$('.simulator-handle').attr('src', single_door_ladder); 
});

$('.sd-square').on('click', function() { 
		$inner_preloader.show();
			setTimeout(function() {
				$inner_preloader.fadeOut(1000);
			}, 3000);
		$('.options.handles img').removeClass("active");
        $('.sd-square img').addClass("active");	       
		$('.simulator-handle').attr('src', layout1_handle); 
});









